const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
const { exchangeRates } = require('../src/util');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:5001').get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})
	
	it('test_api_get_rates_returns_object_of_size_5', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(Object.keys(res.body.rates)).does.have.length(5);
			done();	
		})		
	})

	it('1_test_api_post_currency_returns_200_if_complete_input_given', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
			rates: exchangeRates
		})
		.end((err, res) => {
			expect(res.status).to.equal(200)
            done();
		})
            
	})

	it('2_test_api_post_currency_return_400_if_no_name', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
			name: '',
			ex: {
				peso: 50.73,
				won: 1187.24,
				yen: 108.63,
				yuan: 7.03
			},
			alias: "usd"
			
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		});
	})

	it('3_test_api_post_currency_return_400_if_name_is_not_string', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
			name: 1,
			ex: {
				peso: 50.73,
				won: 1187.24,
				yen: 108.63,
				yuan: 7.03
			},
			alias: "usd"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		});
	})

	it('4_test_api_post_currency_return_400_if_name_is_empty', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
			name: "",
			ex: {
				peso: 50.73,
				won: 1187.24,
				yen: 108.63,
				yuan: 7.03
			},
			alias: "usd"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		});
	})

	it('5_test_api_post_currency_return_400_if_no_ex', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
			name: "United States Dollar",
			alias: "usd"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		});
	})
	
	it('6_test_api_post_currency_return_400_if_no_ex_is_not_object', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
			name: "United States Dollar",
			ex: {
				"peso": 50.73,
				"won": 1187.24,
				"yen": 108.63,
				"yuan": 7.03
			},
			alias: "usd"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		});
	})

	it('7_test_api_post_currency_return_400_if_no_ex_is_empty', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
			name: "United States Dollar",
			ex: {
				
			},
			alias: "usd"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		});
	})

	it('8_test_api_post_currency_return_400_if_no_alias', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
			name: "United States Dollar",
			ex: {
				peso: 50.73,
				won: 1187.24,
				yen: 108.63,
				yuan: 7.03
			},
			alias: ""
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		});
	})

	it('9_test_api_post_currency_return_400_if_alias_is_not_string', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
			name: "United States Dollar",
			ex: {
				peso: 50.73,
				won: 1187.24,
				yen: 108.63,
				yuan: 7.03
			},
			alias: 123
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		});
	})

	it('10_test_api_post_currency_return_400_if_name_is_empty', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
			name: "",
			ex: {
				peso: 50.73,
				won: 1187.24,
				yen: 108.63,
				yuan: 7.03
			},
			alias: "usd"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		});
	})

	it('11_test_api_post_currency_return_400_if_duplicate_alias', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
			alias: "usd"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	it('12_test_api_post_currency_return_400_if_no_dupes', (done) => {
		chai.request('http://localhost:5001')
		.post('/rates')
		.send({
			name: "United States Dollar"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

})
