const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.status(200).send({
			rates: exchangeRates
		});
	})

	app.post('/rates', (req,res) => {
		if(exchangeRates){
			return res.status(200).send({
				rates: exchangeRates
			});
		}

		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
                'error': 'Bad Request - missing required parameter NAME'
            })
		}

		if(typeof req.body.name !== String){
			return res.status(400).send({
                'error': 'Bad Request - NAME has to be a string'
            })
		}

		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
                'error': 'Bad Request - missing required parameter EX'
            })
		}

		if(typeof req.body.ex !==Object){
			return res.status(400).send({
                'error': 'Bad Request - EX has to be a Object'
            })
		}

		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
                'error': 'Bad Request - missing required parameter ALIAS'
            })
		}
		
		if(typeof req.body.alias !== String){
			return res.status(400).send({
                'error': 'Bad Request - NAME has to be a string'
            })
		}
		
		if(typeof req.body.alias === req.body.alias){
			return res.status(400).send({
				'error': 'Bad Request - ALIAS should not have any duplicates'
			})
		}

		if(typeof req.body.name === req.body.name){
			return res.status(400).send({
				'error': 'Bad Request - NAME should not have any duplicates'
			})
		}
	})


}
